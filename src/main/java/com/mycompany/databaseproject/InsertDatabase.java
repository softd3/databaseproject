/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Aritsu
 */
public class InsertDatabase {
    public static void main(String[] args) {
        Connection connect = null;
        String url = "jdbc:sqlite:dcoffee.db";
        try {
            connect = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }        
        
        
        String sql = "INSERT INTO category(cat_id,cat_name) VALUES (?,?)";
        try {
            PreparedStatement stmt = connect.prepareStatement(sql);
            stmt.setInt(1, 3);
            stmt.setString(2, "candy");
            int status = stmt.executeUpdate();
            //ResultSet key = stmt.getGeneratedKeys();
            //key.next();
            //System.out.println("" + key.getInt(1));
            
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            
        }
        
        if (connect != null) {
            try {
                connect.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        
    }
}
