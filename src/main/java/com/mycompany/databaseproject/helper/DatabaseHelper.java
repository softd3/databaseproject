/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.databaseproject.helper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aritsu
 */
public class DatabaseHelper {

    private static Connection connect = null;
    private static final String URL = "jdbc:sqlite:dcoffee.db";

    static {
        getConnect();
    }

    public static synchronized Connection getConnect() {
        if (connect == null) {
            try {
                connect = DriverManager.getConnection(URL);
                System.out.println("Connection to SQLite");
            } catch (SQLException ex) {
                Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return connect;
    }

    public static synchronized void close() {
        if (connect != null) {
            try {
                connect.close();
                connect = null;
            } catch (SQLException ex) {
                Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static int getInsertID(Statement stmt) {
        
        try {
            ResultSet key = stmt.getGeneratedKeys();
            key.next();
            return key.getInt(1);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
}
