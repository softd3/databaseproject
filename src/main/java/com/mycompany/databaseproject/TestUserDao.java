/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.databaseproject;

import com.mycompany.databaseproject.dao.UserDao;
import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.databaseproject.model.User;

/**
 *
 * @author Aritsu
 */
public class TestUserDao {
    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        for(User u : userDao.getAll()){
            System.out.println(u);
        }
        
        User admin1 = userDao.get(1);
        
        
        
        
        //insert
        //User newUser = new User("user2","1234",2,"F");
        
        //User insertUser = userDao.save(newUser);
        //insertUser.setGender("M");
        //admin1.setGender("F");
        //userDao.update(admin1);
        //User update = userDao.get(admin1.getId());
        //System.out.println(update);
        
        //userDao.delete(admin1);
        
        //userDao.getAllOrderBy("user_name", "asc");
        
        for(User u : userDao.getAll(" user_name like 'u%'  ", "user_name asc , user_gender desc ")){
            System.out.println(u);
        }
        
        DatabaseHelper.close();
    }
}
