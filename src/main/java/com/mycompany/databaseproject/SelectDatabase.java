/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aritsu
 */
public class SelectDatabase {

    public static void main(String[] args) {
        Connection connect = null;
        String url = "jdbc:sqlite:dcoffee.db";
        try {
            connect = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }        
        
        String sql = "SELECT * FROM category";
        try {
            Statement stmt = connect.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while (rs.next()) {
                System.out.println(rs.getInt("cat_id") + " " + rs.getString("cat_name"));
            }
            
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            
        }
        
        if (connect != null) {
            try {
                connect.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        
    }
}
