/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aritsu
 */
public class ConnectDatabase {

    public static void main(String[] args) {
        Connection connect = null;
        String url = "jdbc:sqlite:dcoffee.db";
        try {
            connect = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (connect != null) {
                try {
                    connect.close();
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }

    }
}
